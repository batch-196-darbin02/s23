let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemons: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function(){
		console.log(this.pokemons[0] + " I choose you!");
	}
}
console.log(trainer);
trainer.talk();

function Pokemon(name,level,health,attack) {
	this.name = name;
	this.level = level;
	this.health = 3*level;
	this.attack = 1.5*level;
	this.tackle = function(enemy) {
		console.log(enemy.name + " tackled " + this.name);
		console.log(this.name + "'s health is now reduced to " + (this.health - enemy.attack));
		/*if(this.health = 0); {
			this.faint();
		}*/
	}
	this.faint = function() {
		console.log(this.name + " has fainted!");
	};
};


let pokemon1 = new Pokemon("Pikachu",12,18,9);
let pokemon2 = new Pokemon("Geodude",25,16,8);
let pokemon3 = new Pokemon("Mewtwo",10,20,10);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon1.tackle(pokemon2);
pokemon2.tackle(pokemon3);
pokemon3.tackle(pokemon1);
